let boardsInformationBasedOnId= require("./callback1.cjs");
let listsBelongToBoard= require("./callback2.cjs");
let cardsBelongsToList= require("./callback3.cjs");

function data(name,array,obj1,obj2){
    try{
    let id=array.reduce((acc,cv)=>{
        if(cv.name===name){
            acc=cv.id;
        }
        return acc;
    },'');

    boardsInformationBasedOnId(id,array,(error,data)=>{
        if(error){
            console.log(error);
        }else{
            console.log(data)
        }

        listsBelongToBoard(id,obj1,(error,data)=>{
            if(error){
                console.log(error);
            }else{
                console.log(data);
            }

            let idData=data.reduce((acc,cv)=>{
                if(cv.name==="Mind" || cv.name==="Space"){
                    acc.push(cv.id);

                }
                return acc;
            },[]);

            idData.forEach((id)=>{
                cardsBelongsToList(id,obj2,(error,data)=>{
                    if(error){
                        console.log(error);
                    }else{
                        console.log(data);
                    }
                })
            })
        })
    })
}catch(error){
    console.log(error);
}

};

module.exports=data;
